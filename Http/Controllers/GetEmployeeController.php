<?php

namespace Modules\Company\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Company\Entities\Company;

class GetEmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function getEmployee()
    {
        return view('company::companies.getEmployee', [
            'companies' => Company::with('employee')->get(),
        ]);
    }
}
