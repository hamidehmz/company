<?php

namespace Modules\Company\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Company\Entities\Company;
use Spatie\Image\Image;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('company::companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create()
    {
        return view('company::companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, Company $company)
    {
        $validate_data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'slug' => 'unique',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:companies'],
            'website' => ['required', 'max:255'],
            'logo' => 'image',
        ]);
        $company = Company::create([
            'name' => $validate_data['name'],
            'email' => $validate_data['email'],
            'website' => $validate_data['website'],
            'slug' => $validate_data['name'],
            'logo' => $validate_data['logo'],
        ]);
        $company->logo->move('storage', $company->name);
        $path ='storage/'.$company->name;

        $pathlogo = 'storage/'.$company->name.'logo';
        Image::load($path)
            ->width(150)
            ->save($pathlogo);

        $pathsize2 = 'storage/'.$company->name.'size2';
        Image::load($path)
            ->width(300)
            ->save($pathsize2);

        $pathsize3 = 'storage/'.$company->name.'size3';
        Image::load($path)
            ->width(500)
            ->save($pathsize3);



        return redirect()->route('companies.index');
    }

    /**
     * Show the specified resource.
     *
     * @param  Company  $company
     * @return Application|Factory|View
     */
    public function show(Company $company)
    {
        return view('company::companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Company  $company
     * @return Application|Factory|View
     */
    public function edit(Company $company)
    {
        return view('company::companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Company $company)
    {
        $validate_data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'slug' => 'unique',
            'email' => ['required', 'string', 'email', 'max:255'],
            'website' => ['required', 'max:255'],
        ]);

        if ($request['logo']) {
            $company->update([
                'logo' => $request['logo'],
            ]);

            $company->logo->move('storage', $company->name);
        }

        $company->update([
            'name' => $validate_data['name'],
            'email' => $validate_data['email'],
            'website' => $validate_data['website'],
            'slug' => $validate_data['name'],
        ]);

        return redirect()->route('companies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Company  $company
     * @return RedirectResponse
     */
    public function destroy(Company $company)
    {
        $company->delete();

        return redirect()->route('companies.index');
    }
}
