<?php

namespace Modules\Company\Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Company\Entities\Company;

class CompanyDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            Company::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'website' => $faker->url,
            ]);
        }
    }
}
