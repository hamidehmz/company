@extends('company::layouts.app')

@section('content')
    @if($errors->any())
        <div class="alert alert-danger" >
            <ul>
                @foreach($errors->all() as $error)
                    <li>
                        {{$error}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container mt-2">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Create New Company</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('companies.index') }}"> Back</a>
                </div>
            </div>
        </div>

        <div class="container">
            <form action="{{route('companies.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="col-md-2"></div>

                    <div class="form-group col-md-4">
                        <label for="name">Company Name:</label>
                        <input type="text" name="name" class="form-control" autofocus>
                    </div>


                    <div class="form-group col-md-4">
                        <label for="email">Company Email:</label>
                        <input type="email" name="email" class="form-control" >
                    </div>

                    <div class="col-md-2"></div>

                    <div class="form-group col-md-4">
                        <label for="website">Company Website:</label>
                        <input type="text" name="website" class="form-control" >
                    </div>

                    <div class="form-group col-md-4">
                        <label for="logo">Company Logo:</label>
                        <input type="file" name="logo" class="form-control" >
                    </div>

                    <div class="col-md-2"></div>


                    <div class="form-group col-md-4">
                        <button type="submit" class="btn btn-primary ml-3">Submit</button>                        <div class="col-md-2"></div>
                    </div>
                </div>
            </form>
        </div>
@endsection
