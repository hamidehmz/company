@extends('company::layouts.app')

@section('content')
    @if($errors->any())
        <div class="alert alert-danger" >
            <ul>
                @foreach($errors->all() as $error)
                    <li>
                        {{$error}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container mt-2">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Edit Company</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('companies.index') }}" > Back</a>
                </div>
            </div>
        </div>
        <form action="/companies/{{$company->id}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('put') }}


            <div class="form-row">
                <div class="col-md-2"></div>

                <div class="form-group col-md-4">
                    <label for="name">name</label>
                    <input type="text" name="name" class="form-control" value="{{$company->name}}">
                </div>

                <div class="form-group col-md-4">
                    <label for="email">email</label>
                    <input type="email" name="email" class="form-control" value="{{$company->email}}">
                </div>

                <div class="col-md-2"></div>


            </div>

            <div class="form-row">
                <div class="col-md-2"></div>

                <div class="form-group col-md-4">
                    <label for="website">website</label>
                    <input type="text" name="website" class="form-control" value="{{$company->website}}">
                </div>


            </div>

            <div class="form-row">
                <div class="col-md-2"></div>

                <div class="form-group col-md-4">
                    <label for="logo">logo</label>
                    <input type="file" name="logo" class="form-control" placeholder="Company logo">
                    <img src="{{asset('/storage/'.$company->name)}}" height="50" width="50">

                </div>


                <div class="form-group col-md-4">
                    <button class="btn btn-info">update</button>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </form

@endsection
