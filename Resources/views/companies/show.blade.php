@extends('company::layouts.app')

@section('content')

    <div class="container">
        <div class="container mt-2">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Company Information</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('companies.index') }}"> Back</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12"></div>

                <div class="panel panel-default table-responsive">

                    <table class="table table-condensed">
                        <thead>
                        <tr>

                            <th class="text-center">Company_Name</th>
                            <th class="text-center">Company_Email</th>
                            <th class="text-center">Company_Website</th>
                            <th class="text-center">Company_Logo</th>


                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-center">{{ $company->name }}</td>
                            <td class="text-center">{{ $company->email }}</td>
                            <td class="text-center">{{ $company->website }}</td>
                            <td class="text-center"><img src="{{asset('/storage/'.$company->name.'logo')}}"></td>


                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>


@endsection
