<?php

use Illuminate\Support\Facades\Route;
use Modules\Company\Http\Controllers\CompanyController;

Route::resource('companies', CompanyController::class)->middleware('auth')->middleware('verified');
Route::get('/getEmployees', [\Modules\Company\Http\Controllers\GetEmployeeController::class, 'getEmployee'])->name('companies.getEmployees');
