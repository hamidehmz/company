<?php

namespace Modules\Company\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Employee\Entities\Employee;

class Company extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'email', 'website', 'logo', 'company_id'];

    public function employee()
    {
        return $this->hasMany(Employee::class, 'company_id', 'id');
    }
}
